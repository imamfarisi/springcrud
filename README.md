# crudspring

contoh crud menggunakan spring mvc hibernate mysql

Spesifikasi : Java 8 dan MySQL Database
 
Cara Menjalankan
-
- Buat database di MySQL, contoh **barang**
- Config dan sesuaikan terutama bagian user dan password database file **jdbc.properties** di src/main/webapp/WEB-INF/jdbc
- Build aplikasi dengan Maven hingga terbentuk *.war file
- Letakkan/Deploy *.war file tersebut di apps server contoh tomcat
- Done

Tested On
-
- Tomcat 8.5.35
- Glassfish 4.1.1 (hapus dan ganti lib jboss-logging.jar dg jboss-logging-3.3.0.Final.jar di {glassfishdir}/modules)
- Oracle WebLogic Server 12.2.1.3
- IBM Was Liberty Profile (WLP) 19.0.0.4