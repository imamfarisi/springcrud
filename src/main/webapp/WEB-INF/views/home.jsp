<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="contextName" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>Stock Barang</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h1 align="center">Stock Barang</h1>
			</div>
		</div>
	</div>

	<div class="span12">
		<button class="btn btn-primary" id="add" onclick="onClickAdd()">ADD</button>
	</div>
	<br />
	<div class="table-responsive">
		<table id="tables" class="display"></table>
	</div>

	<!-- Modal -->
	<div id="modalBarang" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Stock Barang</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form id="formBarang">
								<div class="box-body">
									<input type="hidden" name="id" id="id">
									<div class="form-group">
										<label>Kode Barang</label>
										<div>
											<input type="text" name="kdBarang" id="kdBarang" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label>Nama Barang</label>
										<div>
											<input type="text" name="nmBarang" id="nmBarang" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label>Jumlah Barang</label>
										<div>
											<input type="number" name="jmlBarang" id="jmlBarang" class="form-control">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<script>
		$(function() {

			$.fn.fromJSON = function(json_string) {
				var $form = $(this)
				var data = JSON.parse(json_string)

				$.each(data, function(key, value) {
					var $elem = $('[name="' + key + '"]', $form)
					$elem.val(value)
				});
			};

			initTables();

		});

		function onClickAdd() {
			$("#formBarang :input").val('');
			$("#modalBarang").modal("show");
		}

		function initTables() {
			var tableId = "#tables";
			if ($.fn.DataTable.isDataTable(tableId)) {
				var currentDataTable = $(tableId).DataTable();
				currentDataTable.destroy();
				$(tableId).empty();
			}

			$('#tables')
					.DataTable(
							{
								ajax : '${contextName}/barang/get-all-barang.json',
								columns : [ {
									data : "kdBarang",
									title : "Kode Barang"
								}, {
									data : "nmBarang",
									title : "Nama Barang"
								}, {
									data : "jmlBarang",
									title : "Jumlah Barang"
								}, {
									data : "createdDate",
									title : "Created Date"
								}, {
									data : "updatedDate",
									title : "Updated Date"
								}, {
									data : null,
									title : "Action"
								} ],
								"columnDefs" : [ {
									"targets" : [ 5 ],
									"data" : "id",
									"render" : function(data) {
										return '<div><button onClick=updateBrg(this)>UPDATE</button>&nbsp;<button onClick=deleteBrg(this)>DELETE</button></div>';
									}
								} ]
							});
		}

		function validateField() {
			var kdBrg = $('#formBarang #kdBarang').val();
			var nmBrg = $('#formBarang #nmBarang').val();
			var jmlBrg = $('#formBarang #jmlBarang').val();

			return kdBrg != "" && nmBrg != "" && jmlBrg != "";
		}

		function save() {
			if (validateField()) {
				var id = $('#formBarang #id').val();
				var url = "${contextName}/barang/save.json";

				if (id != null && id != "") {
					url = "${contextName}/barang/update.json";
				}

				$.ajax({
					url : url,
					type : "POST",
					data : $("#formBarang").serializeArray(),
					success : function(data) {
						$("#formBarang :input").val('');
						$("#modalBarang").modal("hide");
						alert(data.message);
						initTables();
					}
				})
			} else {
				alert("Semua Field Harus Diisi");
			}
		}

		function updateBrg(a) {
			var tr = $(a).closest('td').parent();
			var data = $('#tables').DataTable().row(tr).data();

			$("#formBarang").fromJSON(JSON.stringify(data));
			$("#modalBarang").modal('show');
		}

		function deleteBrg(a) {
			var tr = $(a).closest('td').parent();
			var data = $('#tables').DataTable().row(tr).data();

			$.ajax({
				url : "${contextName}/barang/delete.json",
				type : "POST",
				data : data,
				success : function(data) {
					$("#modalBarang").modal("hide");
					alert(data.message);
					initTables();
				}
			})
		}

	</script>
</body>
</html>