package com.imamfarisi.crud.dao;

import java.util.List;
import java.util.Map;

import com.imamfarisi.crud.model.Barang;

public interface BarangDao {
	void insert(Barang barang) throws Exception;

	void update(Barang barang) throws Exception;

	void delete(Barang barang) throws Exception;

	Barang getBarangById(Long id) throws Exception;

	List<Map<String, Object>> getAllData() throws Exception;
}
