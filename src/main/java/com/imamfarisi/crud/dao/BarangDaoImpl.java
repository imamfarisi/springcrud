package com.imamfarisi.crud.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.imamfarisi.crud.SessionHibernate;
import com.imamfarisi.crud.model.Barang;
import com.imamfarisi.crud.util.Utils;

@Repository
public class BarangDaoImpl extends SessionHibernate implements BarangDao {

	@Override
	public void insert(Barang barang) throws Exception {
		sess().save(barang);
	}

	@Override
	public void update(Barang barang) throws Exception {
		sess().update(barang);
	}

	@Override
	public void delete(Barang barang) throws Exception {
		sess().delete(barang);
	}

	@Override
	public Barang getBarangById(Long id) throws Exception {
		return (Barang) sess().get(Barang.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getAllData() throws Exception {
		String query = " select id, kd_barang, nm_barang, jml_barang, "
				+ " DATE_FORMAT(created_date, '%d-%M-%Y %H:%i:%s'), "
				+ " DATE_FORMAT(updated_date, '%d-%M-%Y %H:%i:%s') "
				+ " from tb_m_barang"
				+ " order by id desc ";
		
		List<Object[]> objectList = sess().createSQLQuery(query).list();
		List<Map<String, Object>> detailMapList = Utils.convertToListOfMap(objectList, "id", "kdBarang", "nmBarang", "jmlBarang",
				"createdDate", "updatedDate");
		return detailMapList;
	}

}