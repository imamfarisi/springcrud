package com.imamfarisi.crud.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = Barang.TABLE_NAME)
public class Barang implements Serializable {
	private static final long serialVersionUID = -8008361304227529868L;
	public static final String TABLE_NAME = "tb_m_barang";
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
	@TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(name = "kd_barang", unique = true, nullable = false)
	private String kdBarang;

	@Column(name = "nm_barang")
	private String nmBarang;

	@Column(name = "jml_barang")
	private Integer jmlBarang;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKdBarang() {
		return kdBarang;
	}

	public void setKdBarang(String kdBarang) {
		this.kdBarang = kdBarang;
	}

	public String getNmBarang() {
		return nmBarang;
	}

	public void setNmBarang(String nmBarang) {
		this.nmBarang = nmBarang;
	}

	public Integer getJmlBarang() {
		return jmlBarang;
	}

	public void setJmlBarang(Integer jmlBarang) {
		this.jmlBarang = jmlBarang;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}