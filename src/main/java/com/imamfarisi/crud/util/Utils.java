package com.imamfarisi.crud.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {
	public static List<Map<String, Object>> convertToListOfMap(List<Object[]> list, String... columnNames) {
		if (list == null) {
			return null;
		}

		if (list.size() > 0) {
			if (list.get(0).length > columnNames.length) {
				return null;
			}
		}

		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for (Object[] item : list) {
			Map<String, Object> temp = new HashMap<String, Object>();
			for (int i = 0; i < columnNames.length; i++) {
				temp.put(columnNames[i], item[i]);
			}
			result.add(temp);
		}
		return result;
	}
}