package com.imamfarisi.crud.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.imamfarisi.crud.dao.BarangDao;
import com.imamfarisi.crud.model.Barang;

@Service
@Transactional
public class BarangServiceImpl implements BarangService {

	@Autowired
	private BarangDao barangDao;

	@Override
	public void insert(Barang barang) throws Exception {
		barangDao.insert(barang);
	}

	@Override
	public void update(Barang barang) throws Exception {
		barangDao.update(barang);
	}

	@Override
	public void delete(Barang barang) throws Exception {
		barangDao.delete(barang);
	}

	@Override
	public Barang getBarangById(Long id) throws Exception {
		return barangDao.getBarangById(id);
	}

	@Override
	public List<Map<String, Object>> getAllData() throws Exception {
		return barangDao.getAllData();
	}
}
