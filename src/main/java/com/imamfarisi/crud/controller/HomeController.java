package com.imamfarisi.crud.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.imamfarisi.crud.model.Barang;
import com.imamfarisi.crud.service.BarangService;

@Controller
public class HomeController {

	@Autowired
	private BarangService barangService;

	@RequestMapping("/")
	public String home() {
		return "home";
	}

	@RequestMapping("/barang/get-all-barang")
	public String getAllData(Model model) {
		try {
			List<Map<String, Object>> listBarang = barangService.getAllData();
			model.addAttribute("success", true);
			model.addAttribute("data", listBarang);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("success", false);
			model.addAttribute("message", "Failed");
		}
		return null;
	}

	@RequestMapping("/barang/save")
	public String save(@ModelAttribute Barang barang, Model model) {
		try {
			barang.setCreatedDate(new Date());
			barangService.insert(barang);
			model.addAttribute("success", true);
			model.addAttribute("message", "Add Successfully");
		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Add Failed");
		}
		return null;

	}

	@RequestMapping("/barang/update")
	public String update(@ModelAttribute Barang barang, Model model) {
		try {
			Barang brg = barangService.getBarangById(barang.getId());
			barang.setCreatedDate(brg.getCreatedDate());
			barang.setUpdatedDate(new Date());
			barangService.update(barang);
			model.addAttribute("success", true);
			model.addAttribute("message", "Update Successfully");
		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Update Failed");
		}
		return null;
	}

	@RequestMapping("/barang/delete")
	public String delete(@ModelAttribute Barang barang, Model model) {
		try {
			Barang brg = barangService.getBarangById(barang.getId());
			barangService.delete(brg);
			model.addAttribute("success", true);
			model.addAttribute("message", "Delete Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("success", false);
			model.addAttribute("message", "Delete Failed");
		}
		return null;
	}

}
