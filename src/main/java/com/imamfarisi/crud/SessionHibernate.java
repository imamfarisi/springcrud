package com.imamfarisi.crud;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class SessionHibernate {
	
	@Autowired
	protected SessionFactory sessionFactory;

	protected Session sess() {
		return this.sessionFactory.getCurrentSession();
	}

}	
